<?php include("includes/header.php"); ?>

<main>
      <section class="container reading">
        <section class="col-md-3">
          <img src="images/pos.png" />
        </section>
        <section class="col-md-9 description">
          <p>
            В данном учебном пособии мною детально рассмотрена одна из важных и, вместе с тем, интересных для понимания клинических дисциплин – биохимия.
            Представлены современные понятия, классификации и иллюстрации важнейших органических соединений в организме человека, таких как белки, липиды и углеводы. Детально рассмотрены их основные обменные биохимические превращения, ежедневно протекающие в пищеварительной системе и обеспечивающие организм энергией.
          </p>
          <p>
            Рекомендуется для использования студентами II курсов по специальностям «Лечебное дело» в предэкзаменационной подготовке.
          </p>
          <a class="btn" href="material/biochemistry/index.html">Читать online</a>
        </section>
      </section>
    </main>

<?php include("includes/footer.php"); ?>

</body>
</html>
