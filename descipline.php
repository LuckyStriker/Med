<?php include("includes/header.php"); ?>

    <main>
      <section class="container">
        <section class="col-md-12 descipline-block">
          <section class="col-md-4">
            <img src="images/desc.jpg"/>
          </section>
          <section class="col-md-8">
            <h4>Общее учение об опухолях</h4>
            <p>
              Предрак - наличие пред-опухолевых процессов (дисплазия, метаплазия, гиперплазия, воспалительные процессы) <br />
              Дисплазия - термин «дисплазия» используется при наличии нарушений роста клеток, что проявляется в виде...
            </p>
            <a class="btn" href="material/synopses/1.pdf">Читать далее</a>
          </section>
        </section>
        <hr />
        <section class="col-md-12 descipline-block">
          <section class="col-md-4">
            <img src="images/desc.jpg"/>
          </section>
          <section class="col-md-8">
            <h4>Опухоли из нервной ткани</h4>
            <p>
              - из центральной (чаще головной, реже спинной мозг), вегетативной, периферической. <br />
              - у взрослых (реже) большинство опухолей – супратенториальные, у детей (чаще) инфратенториальные. <br />
              - могут быть доброкачественными и злокачественными. <br />
              - приводят к тяжелым местным осложнениям и ...
            </p>
            <a class="btn" href="material/synopses/2.pdf">Читать далее</a>
          </section>
        </section>
        <hr />
        <section class="col-md-12 descipline-block">
          <section class="col-md-4">
            <img src="images/desc.jpg"/>
          </section>
          <section class="col-md-8">
            <h4>Опухоли из эпителия</h4>
            <p>
              - наиболее частые среди опухолей. <br />
              Классификация (по гистогенезу): <br />
                1) опухоли из плоского эпителия (многослойного и переходного) <br />
                2) опухоли из железистого. <br />
              Классификация (по течению): ...
            </p>
            <a class="btn" href="material/synopses/3.pdf">Читать далее</a>
          </section>
        </section>
        <hr />
        <section class="col-md-12 descipline-block">
          <section class="col-md-4">
            <img src="images/desc.jpg"/>
          </section>
          <section class="col-md-8">
            <h4>Опухоли кровеносных сосудов</h4>
            <p>
              Группы эндотелиальных сосудистых опухолей: <br />
                1) доброкачественные <br />
                - гемангиомы <br />
                2) пограничные локально агрессивные <br />
                - гемангиоэндотелиома, напоминающая саркому Капоши ...
            </p>
            <a class="btn" href="material/synopses/4.pdf">Читать далее</a>
          </section>
        </section>
        <hr />
        <section class="col-md-12 descipline-block">
          <section class="col-md-4">
            <img src="images/desc.jpg"/>
          </section>
          <section class="col-md-8">
            <h4>Опухоли кроветворной и лимфоидной ткани</h4>
            <p>
              Это лейкозы (системные заболевания кроветворной) и лимфомы (регионарные заболевания кроветворной и/или лимфоидной ткани с
              возможной генерализацией – экстрамедулярные поражения)...
            </p>
            <a class="btn" href="material/synopses/5.pdf">Читать далее</a>
          </section>
        </section>
        <hr />
        <section class="col-md-12 descipline-block">
          <section class="col-md-4">
            <img src="images/desc.jpg"/>
          </section>
          <section class="col-md-8">
            <h4>Опухоли мезенхимального происхождения</h4>
            <p>
              У взрослого человека нет мезенхимы, но из нее произошли: <br />
                - соединительная ткань <br />
                - жировая ткань <br />
                - сосуды ...
            </p>
            <a class="btn" href="material/synopses/6.pdf">Читать далее</a>
          </section>
        </section>
        <hr />
        <section class="col-md-12 descipline-block">
          <section class="col-md-4">
            <img src="images/desc.jpg"/>
          </section>
          <section class="col-md-8">
            <h4>Опухоли меланинобразующей ткани</h4>
            <p>
              - меланинобразующие клетки, содержащие пигмент меланин <br />
              - возникают из шванновской оболочки периферических нервов (нейрогенного происхождения) <br />
              - находится в дерме на базальной мембране ...
            </p>
            <a class="btn" href="material/synopses/7.pdf">Читать далее</a>
          </section>
        </section>
        <hr />
        <section class="col-md-12 descipline-block">
          <section class="col-md-4">
            <img src="images/desc.jpg"/>
          </section>
          <section class="col-md-8">
            <h4>Тератомы</h4>
            <p>
              Тератомы в широком смысле то же, что уродства, однако тератомами принято называть лишь опухолевидные врождённые пороки
              развития у животных и человека, локализованные преимущественно в яичниках, семенниках, реже в других органах ...
            </p>
            <a class="btn" href="material/synopses/8.pdf">Читать далее</a>
          </section>
        </section>
      </section>
    </main>

<?php include("includes/footer.php"); ?>

</body>
</html>
