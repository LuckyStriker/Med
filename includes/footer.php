<footer class="info col-md-12">
    <section class="col-md-12">
        <div class="social col-md-4">
            <h4>Найди меня Вконтакте</h4>
            <a href="https://vk.com/floryamed" class="Vk"><img src="images/Vk.png" width="35"></a>
            <p>Ты всегда можешь написать мне Вконтакте!</p>
        </div>
        <div class="social col-md-4">
            <h4>Найди меня в Instagram</h4>
            <a href="https://www.instagram.com/floryamed" class="Instagram"><img src="images/Instagram.png" width="35"></a>
            <p>Новые фото на #floryamed каждый день!</p>
        </div>
        <div class="social col-md-4">
            <h4>Смотри меня на Youtube</h4>
            <a href="https://www.youtube.com/channel/UCVviAg1Aw7YT-xvHhEiVtLQ" class="Youtube"><img src="images/Youtube.png" width="35"></a>
            <p>Мой Youtube-канал, интересные видео ждут тебя!</p>

        </div>
    </section>
    <section class="subscribe col-md-12">
        <div class="sub">
            <h4>Подпишись!</h4>
            <input placeholder="Email" type="email">
            <input placeholder="Имя" type="text">
            <button class="btn btn-sub">Подписаться</button>
        </div>
    </section>
    <section class="col-md-12 copyright">Все права сохранены. &#169; floryamed. <?php echo date("Y"); ?></section>
</footer>
</div>