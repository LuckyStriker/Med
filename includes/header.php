 <!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>FloryaMed</title>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="./js/jquery.lazyload.mini.js"></script>
    <script src="./js/jquery.colorbox-min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/owl.theme.default.css"/>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/colorbox.css">
</head>

<body>
<div class="main-container">
    <header>
        <section class="news-star">
            <a href="news.php">Новости старостата</a>
        </section>
        <section class="header-section">
            <a href="main.php" class="logo"><img src="images/logo.png"></a>
            <section class="menu">
                <a href="descipline.php">Дисциплина</a>
                <a href="development.php">Учебно-методическая разработка</a>
                <a href="allowance.php">Учебное пособие</a>
                <a href="materials.php">Учебно-демонстрационный материал</a>
                <a href="projects.php" id="fak">Факультетские проекты</a>
            </section>
        </section>
    </header>