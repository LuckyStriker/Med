<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/index.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/animate.css" />
    <link rel="stylesheet" type="text/css" href="css/smartscroll.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/viewportchecker.js"></script>
    <script src="js/smartscroll.min.js"></script>
    <title>FloryaMed</title>
</head>
<body id="top" class="section-wrapper">
<!--[if lt IE 9]>
<script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src = "http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<header class="section">
    <div class="cont_header">
        <nav class="header_nav">
            <div class="lang">
                <a href="index.php">
                    <img class="ru-lang selected" src="./images/ru.png">
                </a>
                <a href="index_en.php">
                    <img class="ru-lang selected" src="./images/en.png">
                </a>
            </div>
            <ul class="header_ulnav">
                <li class="first">
                    <a href="#proj">О проекте</a>
                </li>
                <li class="last">
                    <a href="#about">Обо мне</a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="header-section">
        <div class="button-section">
            <a href="main.php" class = "sheer-button">НАЧАТЬ</a>
        </div>
    </div>
    <a href="#more" class="learn-more">Узнать больше<span class="glyphicon glyphicon-chevron-down"></span></a>
</header>
<div class = "main">
    <section class="block_learn section">
        <div class="container post" id="more">
            <div class="caption">
                <h3 class="cap"><b>Основная идея заключается в том, что...</b></h3>
                <img src="images/dividers-cut_02.png"/>
            </div>
            <div class="text">
                <p>Основная идея заключается в том, что я сосредоточился не на результате, а на работе ради команды.
                    Команды студентов медицинского факультета.</p>
                <br>
                <p>Получается, нахожусь в положении лицом к лицу с громадным личным вызовом. Но,
                    поскольку мой разум прогнозирует позитивный долгосрочный результат – я готов на себя взять смелость,
                    чтобы стремиться к этому.</p>
                <br>
                <p>Смелость – это важный элемент сознательной жизни, именно она даёт возможность решиться на
                    долгосрочный рост перед лицом краткосрочных трудностей. Без достаточной смелости,
                    стандартным поведением будет вести осторожную игру, оказывая предпочтение фальшивой безопасности,
                    а не целенаправленным действиям.</p>
            </div>
        </div>
    </section>
    <section class="block_project section" id="proj">
        <div class="container post">
            <div class="caption">
                <h3 class="cap"><b>История зарождения проекта...</b></h3>
                <img src="images/dividers-cut_02.png"/>
            </div>
            <div class="text">
                <p>История зарождения проекта берет свое начало с 2014 года, когда, будучи старостой курса и
                    представителем актива медицинского факультета ПГУ им. Т.Г. Шевченко, я начал инвестировать много
                    времени на команду факультета. Находясь в активе, мы, совместно с командой, реализовывали
                    большинство проектов, как факультетского, так и университетского уровня. Будучи старостой курса,
                    остальное свободное время уходило на создание учебных пособий и методических разработок,
                    необходимых для качественной подготовки студентов к практическим занятиям.
                </p>
                <br>
                <p>С 2017 года, продолжая оставаться старостой курса и уделяя время команде факультета, хотелось
                    создать собственный проект, который бы позволил поддерживать собственную успеваемость и,
                    тем самым, инвестировать время в учебный процесс. Этим проектом стал <b>"FLORYAMED "</b>.
                    Всегда был убежден, что необходимо поддерживать ощущение страсти к любимому делу и, в то же время,
                    быть частью команды. Основная идея проекта в том, что я сфокусирован как над собственной
                    успеваемостью, так и над работой ради команды.
                </p>
                <br>
                <p>Добро пожаловать и вперед за знаниями!</p>
            </div>
        </div>
    </section>
    <section class="block_me section" id="about">
        <div class="container post">
            <div class="caption">
                <h3 class="cap"><b>Меня часто спрашивают, почему я решил стать врачом...</b></h3>
                <img src="images/dividers-cut_02.png"/>
            </div>
            <div class="text">
                <p>Еще в 2010 году, будет школьником Тираспольской гуманитарно-математической гимназии,
                    я сознательно решил стать врачом. Это решение было пусковым механизмом для изучения биологии,
                    наиболее подходящей дисциплины, на тот период, к медицине. В 11 классе, когда пришло время сдавать
                    единый Государственный экзамен и поступать в высшее учебное заведение, мой выбор пал на
                    Приднестровский Государственный Университет. Сдав ЕГЭ по биологии на 98% и поступив на бюджетную
                    форму обучения, уже на 2 курсе, под руководством доцента, к.б.н., зав. Кафедрой физиологии и
                    фармакологии мы вместе разработали учебно-методическое пособие для лабораторных занятий по
                    физиологии для студентов 2 курса, которое пользуется актуальностью по сегодняшний день.
                </p>
                <br>
                <p>С 2014 года, уделяя внимание команде факультета, я желал поддержать собственную успеваемость.</p>
                <br>
                <p>В 2017 году, это стал учебный проект <b>FLORYAMED</b>.</p>
            </div>
        </div>
    </section>
</div>
<a href="#top" id="back-to-top">
    <img src="images/arrow.png">
</a>
<footer>
    <p><?php echo date("Y"); ?> &#169; Florya. Все права защищены.</p>
</footer>
<script src="js/script.js"></script>
</body>
</html>
