<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/index.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/animate.css" />
    <link rel="stylesheet" type="text/css" href="css/smartscroll.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/viewportchecker.js"></script>
    <script src="js/smartscroll.min.js"></script>
    <title>FloryaMed</title>
</head>
<body id="top">
<!--[if lt IE 9]>
<script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src = "http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <header class="section">
        <div class="cont_header">
            <nav class="header_nav">
                <div class="lang">
                    <a href="index.php">
                        <img class="ru-lang selected" src="./images/ru.png">
                    </a>
                    <a href="index_en.php">
                        <img class="ru-lang selected" src="./images/en.png">
                    </a>
                </div>
                <ul class="header_ulnav">
                    <li>
                        <a href="#proj">About project</a>
                    </li>
                    <li>
                        <a href="#about">About me</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="header-section">
            <section class="button-section">
                <a href="main.php" class = "sheer-button">GET STARTED</a>
            </section>
        </div>
        <a href="#more" class="learn-more">Learn more<span class="glyphicon glyphicon-chevron-down"></span></a>
    </header>
    <div class="main section-wrapper">
            <section class="block_learn section">
                <div class="container post" id="more">
                    <div class="caption">
                        <h3 class="cap"><b>The main idea is...</b></h3>
                        <img src="images/dividers-cut_02.png"/>
                    </div>
                    <div class="text">
                        <p>The main idea is that I focused not on the result, but on the work for the sake of the team.
                            Teams of students of the medical faculty.</p>
                        <br>
                        <p>It turns out that I am in the situation face to face with a huge personal challenge.
                            But, as my mind predicts a positive long-term result - I'm ready to take the liberty to strive
                            for it.</p>
                        <br>
                        <p>Courage is an important element of conscious life, it is precisely it that enables one to decide
                            on long-term growth in the face of short-term difficulties. Without sufficient courage, standard
                            behavior will lead a cautious game, favoring fake security rather than targeted action.</p>
                    </div>
                </div>
            </section>
            <section class="block_project section" id="proj">
                <div class="container post">
                    <div class="caption">
                        <h3 class="cap"><b>The history of the origin of the project...</b></h3>
                        <img src="images/dividers-cut_02.png"/>
                    </div>
                    <div class="text">
                        <p>The history of the origin of the project originates from 2014, when, being the head of the course
                            and the representative of the asset of the medical faculty of the PSU im. T.G. Shevchenko,
                            I began to invest a lot of time in the faculty team. Being in the asset, we, together with the team,
                            implemented most projects, both faculty and university level. As the head of the course,
                            the rest of his spare time was spent on the creation of teaching aids and methodological
                            developments necessary for the qualitative preparation of students for practical studies.
                        </p>
                        <br>
                        <p>Since 2017, continuing to be the head of the course and giving time to the faculty team,
                            I wanted to create my own project that would allow me to maintain my own academic performance and,
                            thus, invest time in the educational process. This project was <b>"FLORYAMED"</b>. I was always convinced
                            that it was necessary to maintain a sense of passion for the beloved cause and, at the same time,
                            be part of the team. The main idea of ​​the project is that I am focused on both my own progress and
                            work for the sake of the team.
                        </p>
                        <br>
                        <p>Welcome and forward for knowledge!</p>
                    </div>
                </div>
            </section>
            <section class="block_me section" id="about">
                <div class="container post">
                    <div class="caption">
                        <h3 class="cap"><b>I am often asked why I decided to become a doctor...</b></h3>
                        <img src="images/dividers-cut_02.png"/>
                    </div>
                    <div class="text">
                        <p>Back in 2010, as a student of the Tiraspol Humanitarian and Mathematical Gymnasium,
                            I deliberately decided to become a doctor. This decision was the trigger mechanism to study biology,
                            the most approximate discipline, at that time, to medicine. In the 11th grade, when it was time to
                            pass a single state examination and enter a higher educational institution, my choice fell on the
                            Transnistrian State University. Having passed the USE on biology by 98% and enrolled in a budgetary
                            form of education, already in the second year, under the guidance of an associate professor,
                            Ph.D., head. Chair of Physiology and Pharmacology, we jointly developed a teaching and
                            methodological manual for laboratory exercises on normal physiology for 2nd year students,
                            which is relevant to the present day.
                        </p>
                        <br>
                        <p>Since 2014, paying attention to the faculty team, I wanted to maintain my own academic performance.</p>
                        <br>
                        <p>In 2017, it became a <b>FLORYAMED</b> training project.</p>
                    </div>
                </div>
            </section>
    </div>
    <a href="#top" id="back-to-top">
        <img src="images/arrow.png">
    </a>
    <footer>
        <p><?php echo date("Y"); ?> &#169; Florya. All rights reserved.</p>
    </footer>
<script src="js/script.js"></script>
</body>
</html>
