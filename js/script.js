/**
 * Created by Аля on 24.04.2017.
 */
$(document).ready(function() {
    //анимация появления
    $('.post').addClass("hidden-block").viewportChecker({
        classToAdd: 'visible animated bounceInUp',
        offset: 100
    });
    //скролл по элементам
    $('a[href^="#"]').click(function (e) {
        e.preventDefault();
        var clickElem = $(this).attr('href');
        var scrollTo = $(clickElem).offset().top;
        $('html, body').animate({ scrollTop: scrollTo}, 1000);
    });
    //появление кнопки
    $(window).scroll(function(){
        const scroll = $("body").scrollTop();
        if ( scroll > 400 ) {
            $("#back-to-top").css("display", "block");
        } else {
            $("#back-to-top").css("display", "none");
        }
    });
    //плавный скролл по блокам
    const options = {
        mode: "vp", // "vp", "set"
        autoHash: false,
        sectionScroll: true,
        initialScroll: true,
        keepHistory: false,
        sectionWrapperSelector: ".section-wrapper",
        sectionClass: "section",
        animationSpeed: 500,
        headerHash: "header",
        breakpoint: null,
        eventEmitter: null,
        dynamicHeight: false
    };
    $.smartscroll(options);
    //увеличение картинки по клику
    $(document).ready(function () {
        $("img").lazyload({effect:"fadeIn"});
        $("a[rel='colorbox']").colorbox({
            maxWidth:"90%",
            maxHeight:"90%",
            opacity:"0.7"
        });
    });
});