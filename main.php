<?php include("includes/header.php"); ?>

    <main>
        <section id="slider" class="owl-carousel">
            <div class="slide">
                <img src="images/1.jpeg">
            </div>
            <div class="slide">
                <img src="images/2.jpeg">
            </div>
            <div class="slide">
                <img src="images/3.jpeg">
            </div>
            <div class="slide">
                <img src="images/4.jpeg">
            </div>
        </section>
        <section class="news col-md-12">
            <h3>Последние новости</h3>
            <div class="col-md-4 news-block">
                <img src="images/news1.jpg" class="news-image">
                <p>Слова «рак», «карцинома» или «саркома» добавляются к названию ткани или органа, из которой произошла опухоль: аденокарцинома –
                    рак из железистого эпителия, плоскоклеточный рак – из многослойного плоского эпителия, переходноклеточный рак – рак из переходного
                    эпителия, фибросаркома – злокачественная опухоль соединительной ткани и т.д </p>
                <button class="btn">Подробнее</button>
            </div>
            <div class="col-md-4 news-block">
                <img src="images/news2.jpg" class="news-image">
                <p>Тератомы в широком смысле то же, что уродства, однако тератомами принято называть лишь опухолевидные врождённые пороки
                    развития у животных и человека, локализованные преимущественно в яичниках, семенниках, реже в других органах. Тератомы похожи
                    на остатки уродливого плода, состоят обычно из всех типов тканей. </p>
                <button class="btn">Подробнее</button>
            </div>
            <div class="col-md-4 news-block">
                <img src="images/news3.jpg" class="news-image">
                <p>В опухолях, как и в нормальных тканях, могут возникать вторичные изменения: дистрофии, некроз, кровоизлияния, воспаление.
                    Дистрофия может быть представлена гиалинозом или ослизнением стромы, а также отложением солей кальция (петрификация).
                    В злокачественных опухолях часто наблюдаются некрозы и кровоизлияния, что придает опухоли пестрый вид</p>
                <button class="btn">Подробнее</button>
            </div>
        </section>
    </main>

<?php include("includes/footer.php"); ?>

<script src="js/slider.js" type = "text/javascript"></script>
</body>
</html>
