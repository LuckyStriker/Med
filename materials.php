<?php include("includes/header.php"); ?>

<div>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#all">Общая медицина</a></li>
        <li><a data-toggle="tab" href="#Psychiatry">Психиатрия</a></li>
        <li><a data-toggle="tab" href="#Pediatrics">Педиатрия</a></li>
        <li><a data-toggle="tab" href="#InternalIllnesses">Внутренние болезни</a></li>
        <li><a data-toggle="tab" href="#Obstetrics">Акушерство</a></li>
        <li><a data-toggle="tab" href="#Gynecology">Гинекология</a></li>
        <li><a data-toggle="tab" href="#Immunology">Иммунология</a></li>
        <li><a data-toggle="tab" href="#Microbiology">Микробиология</a></li>
        <li><a data-toggle="tab" href="#Pharmacology">Фармакология</a></li>
        <li><a data-toggle="tab" href="#Surgery">Хирургия</a></li>
        <li><a data-toggle="tab" href="#SocialSciences">Общественные науки</a></li>
        <li><a data-toggle="tab" href="#Biochemistry">Биохимия</a></li>
        <li><a data-toggle="tab" href="#Anatomy">Анатомия</a></li>
        <li><a data-toggle="tab" href="#Physiology">Физиология</a></li>
        <li><a data-toggle="tab" href="#Pathophysiology">Патофизиология</a></li>
        <li><a data-toggle="tab" href="#Morphopathology">Морфопатология</a></li>
        <li><a data-toggle="tab" href="#Healthcare">Здравохранение</a></li>
        <li><a data-toggle="tab" href="#PropaedeuticsOfInternalDiseases">Пропедевтика внутренних болезней</a></li>
    </ul>

    <div class="tab-content">
        <div id="all" class="tab-pane fade in active">
            <a href="images/materials/1.jpg" rel='colorbox'><img src="images/materials/1.jpg"></a>
            <a href="images/materials/2.jpg" rel='colorbox'><img src="images/materials/2.jpg"></a>
            <a href="images/materials/3.jpg" rel='colorbox'><img src="images/materials/3.jpg"></a>
            <a href="images/materials/4.jpg" rel='colorbox'><img src="images/materials/4.jpg"></a>
            <a href="images/materials/5.jpg" rel='colorbox'><img src="images/materials/5.jpg"></a>
            <a href="images/materials/6.jpg" rel='colorbox'><img src="images/materials/6.jpg"></a>
            <a href="images/materials/7.jpg" rel='colorbox'><img src="images/materials/7.jpg"></a>
            <a href="images/materials/8.jpg" rel='colorbox'><img src="images/materials/8.jpg"></a>
            <a href="images/materials/9.jpg" rel='colorbox'><img src="images/materials/9.jpg"></a>
            <a href="images/materials/10.jpg" rel='colorbox'><img src="images/materials/10.jpg"></a>
            <a href="images/materials/11.jpg" rel='colorbox'><img src="images/materials/11.jpg"></a>
            <a href="images/materials/12.jpg" rel='colorbox'><img src="images/materials/12.jpg"></a>
            <a href="images/materials/13.jpg" rel='colorbox'><img src="images/materials/13.jpg"></a>
            <a href="images/materials/14.jpg" rel='colorbox'><img src="images/materials/14.jpg"></a>
            <a href="images/materials/15.jpg" rel='colorbox'><img src="images/materials/15.jpg"></a>
            <a href="images/materials/16.jpg" rel='colorbox'><img src="images/materials/16.jpg"></a>
            <a href="images/materials/17.jpg" rel='colorbox'><img src="images/materials/17.jpg"></a>
            <a href="images/materials/18.jpg" rel='colorbox'><img src="images/materials/18.jpg"></a>
            <a href="images/materials/19.jpg" rel='colorbox'><img src="images/materials/19.jpg"></a>
            <a href="images/materials/20.jpg" rel='colorbox'><img src="images/materials/20.jpg"></a>
            <a href="images/materials/21.jpg" rel='colorbox'><img src="images/materials/21.jpg"></a>
            <a href="images/materials/22.jpg" rel='colorbox'><img src="images/materials/22.jpg"></a>
            <a href="images/materials/23.jpg" rel='colorbox'><img src="images/materials/23.jpg"></a>
            <a href="images/materials/24.jpg" rel='colorbox'><img src="images/materials/24.jpg"></a>
            <a href="images/materials/25.jpg" rel='colorbox'><img src="images/materials/25.jpg"></a>
            <a href="images/materials/26.jpg" rel='colorbox'><img src="images/materials/26.jpg"></a>
            <a href="images/materials/27.jpg" rel='colorbox'><img src="images/materials/27.jpg"></a>
            <a href="images/materials/28.jpg" rel='colorbox'><img src="images/materials/28.jpg"></a>
        </div>
        <div id="Psychiatry" class="tab-pane fade">
            <p>Психиатрия</p>
        </div>
        <div id="Pediatrics" class="tab-pane fade">
            <p>Педиатрия</p>
        </div>
        <div id="InternalIllnesses" class="tab-pane fade">
            <p>Внутренние болезни</p>
        </div>
        <div id="Obstetrics" class="tab-pane fade">
            <p>Акушерство</p>
        </div>
        <div id="Gynecology" class="tab-pane fade">
            <p>Гинекология</p>
        </div>
        <div id="Immunology" class="tab-pane fade">
            <p>Иммунология</p>
        </div>
        <div id="Microbiology" class="tab-pane fade">
            <p>Микробиология</p>
        </div>
        <div id="Pharmacology" class="tab-pane fade">
            <p>Фармакология</p>
        </div>
        <div id="Surgery" class="tab-pane fade">
            <p>Хирургия</p>
        </div>
        <div id="SocialSciences" class="tab-pane fade">
            <p>Общественные науки</p>
        </div>
        <div id="Biochemistry" class="tab-pane fade">
            <p>Биохимия</p>
        </div>
        <div id="Anatomy" class="tab-pane fade">
            <p>Анатомия</p>
        </div>
        <div id="Physiology" class="tab-pane fade">
            <p>Физиология</p>
        </div>
        <div id="Pathophysiology" class="tab-pane fade">
            <p>Патофизиология</p>
        </div>
        <div id="Morphopathology" class="tab-pane fade">
            <p>Морфопатология</p>
        </div>
        <div id="Healthcare" class="tab-pane fade">
            <p>Здравохранение</p>
        </div>
        <div id="PropaedeuticsOfInternalDiseases" class="tab-pane fade">
            <p>Пропедевтика внутренних болезней</p>
        </div>
    </div>
</div>

<?php include("includes/footer.php"); ?>

<script src="./js/script.js"></script>

</body>
</html>