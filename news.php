<?php include("includes/header.php"); ?>

    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>2 февраля</b></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                    <p class="news-item">Внимание, старосты!
                                        Я все еще жду от Вас ответы, насчет данных вашей группы, где есть студенты (Ф.И.О. , группа и курс), которые являются детьми защитников ПМР и участников боевых действий по защите ПМР.
                                        Те, старосты, кто отписался - всех помню. Жду остальных старост.
                                        P.s по вопросам стипендий и пакета документов к нем - начал этим заниматься. Осведомлю каждого, как буду сам осведомлен. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>31 января</b></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                    <p class="news-item">Внимание!
                                        Кристенко Я., Кулемин И., Павленко В., Эрикс А., Терзи Л., Тащи Т. Свяжитесь со мной.
                                        Есть новости по Вашим вопросам. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>17 января</b></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                    <p class="news-item">Уважаемый 4 курс!
                                        Поздравляю Вас с Новым Годом!
                                        Желаю всем поддерживать ощущение любви!
                                        Не обязательно к человеку, пускай к любимому занятию, делу, не важно. Желаю сознательно решить, какие связи Вам стоит в жизни укреплять, а каким дать ослабнуть. Поскольку в долгосрочной перспективе жизнь станет отражением того, с чем Вы предпочитали связываться наиболее часто. А медики, это те люди, которые заслуживают счастья!

                                        С любовью, Ваш староста курса. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>10 января</b></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                    <p class="news-item">Внимание, старосты!
                                        Завтра, 10.01.17, на кафедре акушерства и гинекологии ждать журналы всех желающих будет Герич Ирина Михайловна!
                                        Кому нужны росписи от неё, сразу после лекции к ней в цМиР.
                                        Назначенное время: 14.00 </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>6 января</b></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                    <p class="news-item">Внимание, старосты!
                                        Вы самые лучшие у меня на курсе!
                                        К Вам меньше всего было претензий с деканата, где подводили итоги уходящего 2016! Спасибо, Вам, что Вы есть и помогаете мне 😘
                                        P.s в сроки 15-20 января Гарбуз Л.И. ждет журналы!<p/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include("includes/footer.php"); ?>

</body>
</html>